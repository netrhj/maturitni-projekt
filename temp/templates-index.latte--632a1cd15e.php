<?php

use Latte\Runtime as LR;

/** source: templates/index.latte */
final class Template_632a1cd15e extends Latte\Runtime\Template
{
	public const Source = 'templates/index.latte';

	public const Blocks = [
		['content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="templates/IndexStyle.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<head>
    <meta charset="UTF-8">
    <title>Piškvorky</title>
</head>
<body>
    <header>
    <h1 id="nadpis">piškvorky</h1>
    </header>

    <!---Hamburger Menu------------------------------------>
    <aside class="sidebar" >
      <nav>
        
        <a href="prihlaseni.php" class="nabidka" id="prihlaseni">přihlášení</a>
        <img id="signUpIcon2" class="signUpIcon" src="./imgs/signupIcon2.png" alt="Ikonka pro přihlášení 2">
        <img id="signUpIcon1" class="signUpIcon" src="./imgs/signupIcon1.png" alt="Ikonka pro přihlášení 1">
        <br>
        <a href="#" class="nabidka">o nás</a>
        <br>
        <a href="#" class="nabidka">kontakt</a>
      </nav>
    </aside>

    <!------------------------------------------------------>
    <main>
';
		$this->renderBlock('content', get_defined_vars()) /* line 34 */;
		echo '    </main>

    <footer>
    </footer>
</body>
</html>';
	}


	/** {block content} on line 34 */
	public function blockContent(array $ʟ_args): void
	{
	}
}
