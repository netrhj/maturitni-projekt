<?php

use Latte\Runtime as LR;

/** source: templates/prihlaseni.latte */
final class Template_80f52170ef extends Latte\Runtime\Template
{
	public const Source = 'templates/prihlaseni.latte';

	public const Blocks = [
		['nadpis' => 'blockNadpis', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		$this->renderBlock('nadpis', get_defined_vars()) /* line 2 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 3 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		$this->parentName = 'layout.latte';
		return get_defined_vars();
	}


	/** {block nadpis} on line 2 */
	public function blockNadpis(array $ʟ_args): void
	{
		echo 'přihlášení';
	}


	/** {block content} on line 3 */
	public function blockContent(array $ʟ_args): void
	{
		echo '<form action="prihlaseni.php" method="get">
    <label for="username">Jméno:</label><br>
    <input type="text"name="jmeno"><br>
    <label for="pwd">Heslo:</label><br>
    <input type="password" name="heslo">
    <br>
    <input type="submit" name="hesloBtn" value="Přihlásit se!">
    <br>
    <span>Nemáte účet? Můžete se <a href="registrace.php">zaregistrovat</a></span>
</form>';
	}
}
